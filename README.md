#demo

Steps to test
1) Start the app
2) Navigate to the below endpoint on the browser <br />
   `http://localhost:8080/hystrix`
3) Specify the below hystrix stream endpoint and click on "Monitor Stream" <br />
   `http://localhost:8080/actuator/hystrix.stream`
4) In a separate tab navigate to the below endpoint to see "Hello World" or "Error Response" <br />
   `http://localhost:8080/hello`
5) Verify that the requests are captured in the dashboard.
